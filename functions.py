#2. Functions

#Create new functions
 
#The following cells with comments are describing a function. Code those functions under the comments. Each function has a test cell to verify how your function works.
#And remember : Online documentation is your best friend ! You will need already existing functions to finish all exercices, like "how to cast a float as an int".

# a) Code a function "hello" with no arguments that prints "Hello World !"




 hello()

# b) Code a function "addition" with two arguments that sums those arguments and returns value of the sum.

a = addition(2,3)
b = addition(5,9)
print(a+b == 19)
c = addition("Hello ","World !")
print(c)


# c) Code a function "multiplication" with two arguments that multiply them and returns the result.

d = multiplication(2,3)
e = multiplication(2,2)
print(multiplication(d,e) == 24)  

# d) Code a function "multi_addition1" with n ints that sums all those ints and returns the value of the sum.  

print(multi_addition1(1,2,3))
print(multi_addition1(10,4,8,9,5))  # Code a function "multi_addition2" with n arguments that sums all those arguments and returns the value of the sum  print(multi_addition1(1))
print(multi_addition1(10,2,8,14,5,78,42))
print(multi_addition1("He","llo ","Wor","ld"," !"))  

# e) Code a function "reverse" with one str as argument that reverse all the chars in this str and returns it.
# For ex, "EPITA" will become "ATIPE"  print(reverse("EPITA"))

print(reverse("Hello World !"))
print(reverse("KayaK"))  

# f) Code a function "neg_pos" with one list as argument that places negative numbers at the beginning of the list 
# and positive numbers at the end of the list, keeping their orders.  

print(neg_pos([1,2,-3,4,-5,6,7,-8,-9]))
print(neg_pos([4,-7,2,0,-8,14,-2,3]))  

# g) Code a function "division" with two int as arguments that returns quotient and prints the remainder. 
# If a division by zero is made, returns None and prints "Error, division by zero"
# Careful ! Remainder and quotient should be ints, not floats.  

print(division(10,3))
print(division(12,0))
print(division(20,5))
print(division(15,6))  

# h) Code a function "median" that returns the element at the median of a list given as argument.
# If the length of the list is even (then median is not an int), then pick the element before the median.
# For ex, median([1,24,6,54]) returns 24  

print(median([1,24,6,54]))
print(median([1,2,3,4,5]))  

# i) Code a function "mean" that returns the mean value of all the elements of a list given as argument.
# For ex, mean([1,24,6,54]) returns 21.25  

print(mean([1,24,6,54]))
print(mean([1,2,3,4,5]))  

# j) Code a function "mean_within" that returns the closest element of a list given as argument to its mean value.
# For ex, mean_within([1,24,6,54]) returns 24  print(mean_within([1,24,6,54]))
print(mean_within([1,2,3,4,5]))
print(mean_within([54,14,98,65,1052,47895,458,12,357,410,2658,48]))
print(mean_within([-48,-875,-7995,-1485,-79522,-478,-1365,-795,-799,-421,-486,-325]))  

# k) Code an iterative function "fact_it" with one int as argument that returns the factorial of n.
# %%time gives the execution time of the cell

print(fact_it(5))
print("\n")
print(fact_it(20))
print("\n")
print(fact_it(100))
print("\n")
print(fact_it(1000))  

# l) Code a recusive function "fact_rec" with one int as argument that returns the factorial of n.  
## This will be needed to go above the limit of max recursions
import sys
sys.setrecursionlimit(2000)   

print(fact_rec(5))
print("\n")
print(fact_rec(20))
print("\n")
print(fact_rec(100))
print("\n")
print(fact_rec(1000))  

# m) Code a function "removal" with two str as arguments that remove in the first str every char in the second str.
# and returns the modified str.
# For ex, removal("abcd","ad") returns "bc"  print(removal("4OvzHabehLAolq3alby0Y WBf11Mk0hZwqYaA5MrzoMlOBhd8vLyZ !","ay8OzMAf4qw1vLBk3hbR5toZY"))

b = "3vZTeZbh3qO5WzNiIAfzoezs6oH4AU j2I65zsA in01jxtg9 3t51JKehAkQeDVat6ur gCec0x2Nmz9Rop5ZilxIoijfic16waDZ3t5D5E8Po592UPu1AFU1Oqbi2FOAi7gKzv91q77zeuiHOjZuN7ju4OA8473oiNuNiLozwr7KUB9jfkw91CuAuoQd75Qr5e"
c = "HCyURP6wzJ4Mru2F5kxY1ZKve7LGgX3ONiBjDf9VqSbAW8oQ"
print(removal(b,c))  

# n) Code a function "fib" with one int as argument that returns the value of the n-th number of Fibonacci.
# Fibonacci(n) = Fibonacci(n-1)+Fibonacci(n-2)  

print("Fibonacci(1) = %s" %fib(1))
print("\n")
print("Fibonacci(2) = %s" %fib(2))
print("\n")
print("Fibonacci(5) = %s" %fib(5))
print("\n")
print("Fibonacci(10) = %s" %fib(10))
print("\n")
print("Fibonacci(50) = %s" %fib(50))
print("\n")
print("Fibonacci(100) = %s" %fib(100))
print("\n")
print("Fibonacci(245) = %s" %fib(245))
print("\n")
print("Fibonacci(385) = %s" %fib(385))
print("\n")
print("Fibonacci(576) = %s" %fib(576))
print("\n")
print("Fibonacci(3029) = %s" %fib(3029))  

# o) Code a function "rand" with one argument that returns a random element of that argument.
# Importing the library "random" could be useful  import random

print(rand('abcdefghijklmnop'))
print(rand([158,1486,864,31,48,43654,6,84,1,3358,48946,478,9512]))  

# p) Code a function with a list and a value V as arguments that displays the index of the second value V in the list.
# For example, with [13 , 15 , 12 , 17 , 15 , 18 , 15 , 17 ] and V = 15, the function returns 4. 
# If V is not included twice in the list, the program displays -1.   

print(ret_two([1,2,3,4,5],1))
print(ret_two([1,2,3,4,1,5,6,7],1))
print(ret_two([1,11,2,3,4,5],1))  

# q) Write a function "stars" with a list as argument that prints the cumulative numbers of the different elements 
# of the list in the form of sticks made up of stars. 
# For example, with [13 , 15 , 12 , 17 , 15 , 18 , 15 , 15 , 17 , 13 , 12 , 15 ] , the program displays :
# **12
# **13
# **15
# **17
# *18  stars([1,1,2,4,3,2,4])

stars([1,1,1,1,1,1,1,1,2,1,3,2,3,2,2,1,2,3,4,3,2,1,1])

# r) Write a function "inter" with two lists as arguments that returns the elements present in the two lists.

print(inter([1,2,3,4,5,6],[5,6,7,8,9]))
print(inter([1,2,7,9,10,35,48,12,23,59,44],[45,2,78,23,56,10,9,84,59,36,35,23]))